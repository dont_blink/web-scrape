from selenium import webdriver
from bs4 import BeautifulSoup
import boatData
from selenium.webdriver.chrome.options import Options

url1 = 'https://www.boat-lifestyle.com/products/rockerz-450'
url2 = 'https://www.boat-lifestyle.com/products/rockerz-185'
url3 = 'https://www.boat-lifestyle.com/products/rockerz-510?variant=20342772858978'

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166"')

#chrome_options.headless = True # also works
driver = webdriver.Chrome(options=chrome_options,executable_path='c:\chromedrivers\chromedriver.exe')

driver.get(url1)

html = driver.page_source
driver.quit()

soup = BeautifulSoup(html, "html.parser")
details = []
details = boatData.giveInfo(soup)

# print(details[0].brandName)
# print(details[0].productName)
# print(details[0].productPrice)
# print(details[0].review)
# print(details[0].ratings)
# print(details[0].image)
