from bs4 import BeautifulSoup

details = []

class Info:
    def __init__(self, brandName, productName, productPrice, review, ratings, image):
        self.brandName = brandName
        self.productName = productName
        self.productPrice = productPrice
        self.review = review
        self.ratings = ratings
        self.image = image

def giveInfo(soup):
    container = soup.find("div", {"class": "product-detail"})

    #Image Section data..
    cont = container.find("div",{"class":"slide slick-slide slick-current slick-active"})
    image ='https:' + cont['data-full-size-url']

    # #Product details section..
    comp = container.find("div",{"class":"detail product-column-right gallery-layout-beside"})
    brand = comp.find("h1",{"class":"title product-title"})
    brandName = brand.text
    product = comp.find("p",{"class":"product-type text-uppercase"})
    productName = product.text
    price = comp.find("span",{"class":"current-price theme-money"})
    productPrice = price.text
    pdreview = comp.find("span",{"class":"jdgm-prev-badge__text"})
    review = pdreview.text
    ratings = None
    #print(brandName,productName, productPrice, review, ratings)
    details.append(Info(brandName,productName,productPrice,review,ratings,image))
    return details