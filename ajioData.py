from bs4 import BeautifulSoup

details = []

class Info:
    def __init__(self, brandName, productName, productPrice, review, ratings, image):
        self.brandName = brandName
        self.productName = productName
        self.productPrice = productPrice
        self.review = review
        self.ratings = ratings
        self.image = image

def giveInfo(soup):
    container = soup.find("div", {"class": "prod-container"})
    #Image Section data..
    cont = container.find("div",{"class":"img-container"})
    image = cont.img['src']

    #Product details section..
    comp = container.find("div",{"class":"prod-content"})
    brand = comp.find("h2",{"class":"brand-name"})
    brandName = brand.text
    product = comp.find("h1",{"class":"prod-name"})
    productName = product.text
    price = comp.find("div",{"class":"prod-sp"})
    productPrice = price.text
    review = None
    ratings = None
    details.append(Info(brandName,productName,productPrice,review,ratings,image))
    return details